package org.hpdragon.naturallanguageprocessing.semantics.namedentityrecognition;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @brief   named entity recognition
 */
public class NamedEntityRecognition {

    private HiddenMarkovModel hmm;

    //learned data will be stored here
    private List<String> learnData;

    //leared data that was loaded from memory will be stored here
    private List<String> learnedMemory;

    /**
     * @brief   constructor
     */
    public NamedEntityRecognition() {
        hmm = new HiddenMarkovModel();

        learnedMemory = new ArrayList<>();
    }

    /**
     * @brief   extract data from file
     * @param   file
     * @throws  IOException
     */
    public void learn(File file) throws IOException {
        FileReader fileReader = new FileReader(file);
        BufferedReader br = new BufferedReader(fileReader);

        String line;
        String[] linePart;

        while((line = br.readLine()) != null) {
            linePart = line.split("\t");

            hmm.add(linePart);
        }

        learnData = hmm.buildGrams();
    }

    /**
     * @brief   save learned data to file
     * @param   file
     * @throws  IOException
     */
    public void saveLearnedData(File file) throws IOException {
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bf = new BufferedWriter(fileWriter);
        for(int i = 0; i < learnData.size(); i++) {
            bf.write(learnData.get(i) + "\n");
        }
        bf.flush();
    }

    /**
     * @brief   load memory from file
     * @param   file
     * @throws  IOException
     */
    public void loadMemory(File file) throws IOException {
        FileReader fileReader = new FileReader(file);
        BufferedReader br = new BufferedReader(fileReader);

        String line;
        while((line = br.readLine()) != null) {
            learnedMemory.add(line);
        }

        hmm.getMemory(learnedMemory);
    }

    /**
     * @brief
     * @param   words
     * @return
     */
    public String[] tag(String[] words) {
        return hmm.tag(words);
    }

}
