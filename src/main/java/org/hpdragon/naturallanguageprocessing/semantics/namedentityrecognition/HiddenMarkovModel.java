package org.hpdragon.naturallanguageprocessing.semantics.namedentityrecognition;

import java.util.*;
import java.util.concurrent.CountDownLatch;

/**
 * @brief   hidden Markov model
 */
public class HiddenMarkovModel {

    //constants in data
    private static final String UNI_GRAM = "1-Gram";
    private static final String BI_GRAM = "2-Gram";
    private static final String TRI_GRAM = "3-Gram";
    private static final String WORDTAG_GRAM = "WordTag";

    //training data
    private List<String> lWord;
    private List<String> lLabel;
    //training data tags
    private static String END_OF_COPUS_TAG = "_EOC";
    private static String OTHER_COPUS_TAG = "O";

    //data for unigram
    //TODO  change structure of unigram because of it needs to be follow order in order to used by initialTransmission method
    private HashMap<String, Integer> uniGramData;

    //data for bigram
    private HashMap<String, Integer> biGramData;

    //data for trigram
    private HashMap<String, Integer> triGramData;

    //data for wordtag
    private HashMap<String, Integer> wordTagData;

    //emission, transmission and possibility data
    private List<List<Float>> emission;
    private List<List<List<Float>>> tranmission;

    /**
     * @brief   constructor
     */
    public HiddenMarkovModel() {
        //data for learning purpose
        lWord = new ArrayList<>();
        lLabel = new ArrayList<>();

        //gram things
        uniGramData = new HashMap<>();
        biGramData = new HashMap<>();
        triGramData = new HashMap<>();
        wordTagData = new HashMap<>();

        //possibility things
        emission = new ArrayList<>();
        tranmission = new ArrayList<>();
    }

    /**
     * @brief   add word and label
     * @param   data    [word, label]
     */
    public void add(String[] data) {
        if (data.length == 2) {
            lWord.add(data[0]);
            lLabel.add(data[1]);
        } else {
            lWord.add(END_OF_COPUS_TAG);
            lLabel.add(END_OF_COPUS_TAG);
        }
    }

    /**
     * @brief   count all labels
     * @return
     */
    private int countLabel() {
        return lLabel.size();
    }

    /**
     * @brief   count label similar to label
     * @param   label
     * @return
     */
    private int countLabel(String label) {
        return Collections.frequency(lLabel, label);
    }

    /**
     * @brief   count labels have the oder of appearance: label1 label2
     * @param   label1
     * @param   label2
     * @return
     */
    private int countLabel(String label1, String label2) {
        int c = 0;
        for (int i = 0; i < lLabel.size()-1; i++)
            if (label1.equals(lLabel.get(i)) && label2.equals(lLabel.get(i + 1)))
                c++;
        return c;
    }

    /**
     * @brief   count labels have the oder of appearance: label1 label2 label3
     * @param   label1
     * @param   label2
     * @param   label3
     * @return
     */
    private int countLabel(String label1, String label2, String label3) {
        int c = 0;
        for (int i = 0; i < lLabel.size()-2; i ++)
            if ( label1.equals(lLabel.get(i)) && label2.equals(lLabel.get(i+1)) && label3.equals(lLabel.get(i+2)) )
                c++;
        return c;
    }

    /**
     * @brief   count word that is tagged by label
     * @param   word
     * @param   label
     * @return
     */
    private int countWordTagged(String word, String label) {
        int c = 0;
        for (int i = 0; i < lLabel.size(); i ++)
            if ( label.equals(lLabel.get(i)) && word.equals(lWord.get(i)) )
                c++;

        return c;
    }

    /**
     * @brief   count appearance of each label
     */
    private void buildUniGram() {
        HashSet<String> temp = new HashSet<>();

        String label;
        for (int i = 0; i < lLabel.size(); i ++) {
            label = lLabel.get(i);

            //check if label was counted
            if (temp.contains(label))
                continue;

            uniGramData.put(label, countLabel(label));
            temp.add(label);
        }
    }

    /**
     * @brief   count appearance of label B follows by label A (A B)
     */
    private void buildBiGram() {
        HashSet<String> temp = new HashSet<>();
        temp.clear();

        String label, label1, label2;
        for (int i = 0; i < lLabel.size()-1; i++) {
            label1 = lLabel.get(i);
            label2 = lLabel.get(i+1);
            label = label1 + " " + label2;

            //check if label was counted
            if (temp.contains(label)) {
                continue;
            }

            biGramData.put(label, countLabel(label1, label2));
            temp.add(label);
        }
    }

    /**
     * @brief   count appearance of label C follows by label A and B (A B C)
     */
    private void buildTriGram() {
        HashSet<String> temp = new HashSet<>();

        String label, label1, label2, label3;
        for (int i = 0; i < lLabel.size()-2; i++) {
            label1 = lLabel.get(i);
            label2 = lLabel.get(i+1);
            label3 = lLabel.get(i+2);
            label = label1 + " " + label2 + " " +label3;

            //check if label was counted
            if (temp.contains(label))
                continue;

            triGramData.put(label, countLabel(label1, label2, label3));
            temp.add(label);
        }
    }

    /**
     * @brief   thread for count word tag
     */
    class WordTagThread implements Runnable {

        private final int threadNumber;
        private final int numberOfThread;
        private final int startNumber;
        private final int endNumber;

        private final CountDownLatch latch;

        private HashSet<String> countedWordTag;

        public WordTagThread(int threadNumber, int numberOfThread, HashSet<String> counted, CountDownLatch latch) {
            this.threadNumber = threadNumber;
            this.numberOfThread = numberOfThread;
            this.countedWordTag = counted;
            this.latch = latch;

//                endNumber = (int) (lLabel.size()/((float)numberOfThread/threadNumber) );
//                startNumber = endNumber*(threadNumber-1);

            switch (threadNumber) {
                case 1:
                    if (this.numberOfThread == 1)
                        endNumber = lLabel.size();
                    else
                        endNumber = lLabel.size()/this.numberOfThread;
                    startNumber = 0;
                    break;
                case 2:
                    if(this.numberOfThread == 2)
                        endNumber = lLabel.size();
                    else
                        endNumber = (lLabel.size()/this.numberOfThread)*this.threadNumber;

                    startNumber = lLabel.size()/this.numberOfThread;
                    break;
                case 3:
                    endNumber = (lLabel.size()/this.numberOfThread)*this.threadNumber;
                    startNumber = (lLabel.size()/this.numberOfThread)*2;
                    break;
                case 4:
                    endNumber = lLabel.size();
                    startNumber = (lLabel.size()/this.numberOfThread)*3;
                    break;
                default:
                    endNumber = lLabel.size();
                    startNumber = endNumber;
                    break;
            }
        }

        @Override
        public void run() {
            String wordTag, word, label;
            for (int i = startNumber; i < endNumber; i++) {
                word = lWord.get(i);
                label = lLabel.get(i);
                wordTag = word + " " + label;

                //check if label was counted
                if (countedWordTag.contains(wordTag))
                    continue;

                wordTagData.put(wordTag, countWordTagged(word, label));
                countedWordTag.add(label);
            }

            this.latch.countDown();
        }
    }

    /**
     * @brief   count appearance of word W follows by tag T
     *          note:   currently just support 2 and 4 threads
     */
    private void buildWordTag(int numberOfThread) {
        if (numberOfThread != 2 && numberOfThread != 4)
            numberOfThread = 2;

        System.out.println("-->> " + "Word Tag will generate " + numberOfThread + " threads to count word tag");

        Thread[] wordTagThread = new Thread[numberOfThread];
        HashSet<String> countedWordTag = new HashSet<>();
        CountDownLatch latch = new CountDownLatch(numberOfThread);

        for (int i = 0; i < wordTagThread.length; i++) {
            wordTagThread[i] = new Thread(new WordTagThread(i+1, numberOfThread, countedWordTag, latch));
        }

        double old = System.currentTimeMillis();

        for (Thread t : wordTagThread) {
            t.start();
        }

        try {
            //wait for calculation to finish
            latch.await();
            System.out.println("-->> " + "costed " + (System.currentTimeMillis() - old) + " to build Word Tag");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * @brief   build uni, bi, tri gram and wordtag
     * @return
     */
    public List<String> buildGrams() {
        //clear the way
        uniGramData.clear();
        biGramData.clear();
        triGramData.clear();
        wordTagData.clear();

        List<String> ret = new ArrayList<>();

        System.out.println("--> Building unigram");
        buildUniGram();
        System.out.println("--> Building bigram");
        buildBiGram();
        System.out.println("--> Building trigram");
        buildTriGram();
        System.out.println("--> Building wordtag");
        buildWordTag(4);

        System.out.println("--> Exporting data");
        for(HashMap.Entry m : uniGramData.entrySet())
            ret.add(UNI_GRAM + " " + m.getValue() + " " + m.getKey());
        for(HashMap.Entry m : biGramData.entrySet())
            ret.add(BI_GRAM + " " + m.getValue() + " " + m.getKey());
        for(HashMap.Entry m : triGramData.entrySet())
            ret.add(TRI_GRAM + " " + m.getValue() + " " + m.getKey());
        for(HashMap.Entry m : wordTagData.entrySet())
            ret.add(WORDTAG_GRAM + " " + m.getValue() + " " + m.getKey());
        System.out.println("--> Done");

        return ret;
    }

    /**
     * @brief   get memory from data
     * @param   memory
     */
    public void getMemory(List<String> memory) {
        //clear the way
        uniGramData.clear();
        biGramData.clear();
        triGramData.clear();
        wordTagData.clear();

        String[] linePart;
        for (int i = 0; i < memory.size(); i++) {
            linePart = memory.get(i).split(" ");

            if (linePart.length >= 3) {
                switch (linePart[0]) {
                    case UNI_GRAM:
                        uniGramData.put(linePart[2], Integer.valueOf(linePart[1]));

                        break;
                    case BI_GRAM:
                        biGramData.put(linePart[2] + " " + linePart[3], Integer.valueOf(linePart[1]));

                        break;
                    case TRI_GRAM:
                        triGramData.put(linePart[2] + " " + linePart[3] + " " + linePart[4], Integer.valueOf(linePart[1]));

                        break;
                    case WORDTAG_GRAM:
                        wordTagData.put(linePart[2] + " " + linePart[3], Integer.valueOf(linePart[1]));

                        break;
                    default:
                        break;
                }
            }
        }

        List<String> initialLabels = new ArrayList<>(uniGramData.keySet());
        initialTransmission(initialLabels);
    }

    /**
     * @brief
     * @return
     */
    private int countNumberOfLabel() {
        int count = 0;
        for (Map.Entry m : uniGramData.entrySet())
            count += Integer.valueOf(m.getValue().toString());

        return count;
    }

    /**
     * @brief
     * @param   word
     * @param   label
     * @return
     */
    private float e(String word, String label) {
        Object wordTag = wordTagData.get(word + " " + label);
        Object uniGram = uniGramData.get(label);
        if (wordTag != null && uniGram != null)
            return Float.valueOf(wordTag.toString()) / Float.valueOf(uniGram.toString());

        return 0;
    }

    /**
     * @brief
     * @param   label1  y(i)
     * @return
     */
    private float q(String label1) {
        Object uniGram = uniGramData.get(label1);
        if (uniGram != null)
            return Float.valueOf(uniGram.toString()) / countNumberOfLabel();
        return 0;
    }

    /**
     * @brief
     * @param   label2  y(i)
     * @param   label1  y(i-1)
     * @return
     */
    private float q(String label2, String label1) {
        Object uniGram = uniGramData.get(label2);
        Object biGram = biGramData.get(label1 + " " + label2);
        if (uniGram != null && biGram != null)
            return Float.valueOf(biGram.toString()) / Float.valueOf(uniGram.toString());

        return 0;
    }

    /**
     * @brief   => (sLabel_3 | sLabel_1, sLabel_2) = Count (sLabel_1, sLabel_2, sLabel_3)
     * @param   label3  y(i)
     * @param   label1  y(i-2)
     * @param   label2  y(i-1)
     * @return
     */
    private float q(String label3, String label1, String label2) {
        Object biGram = biGramData.get(label1 + " " + label2);
        Object triGram = triGramData.get(label1 + " " + label2 + " " + label3);
        if (biGram != null && triGram != null)
            return Float.valueOf(triGram.toString()) / Float.valueOf(biGram.toString());

        return 0;
    }

    /**
     * @brief   inital an Emission matrix
     * @param   words
     */
    private void initialEmission(List<String> words) {
        List<Float> wordEmission;

        emission.clear();

        for (String word : words) {
            wordEmission = new ArrayList<>();
            for (Map.Entry m : uniGramData.entrySet()) {
                wordEmission.add(e(word, m.getKey().toString()));
            }
            emission.add(wordEmission);
        }
    }

    /**
     * @brief   the idea is to inital a three dimensional matrix contents transmission possibility
     *          v_Transmission[One][Two][Three]	    (One, Two, Three = Number of Position)
     * @param   initialLabels
     */
    private void initialTransmission(List<String> initialLabels) {
        //two
        List<List<Float>> trans1;
        //three
        List<Float> trans2;

        tranmission.clear();

        for (String one : initialLabels) {
            trans1 = new ArrayList<>();

            for (String two : initialLabels) {
                trans2 = new ArrayList<>();

                for (String three : initialLabels) {
                    trans2.add(q(three, one, two));
                }

                trans1.add(trans2);
            }

            tranmission.add(trans1);
        }
    }

    /**
     * @brief   find the position of the maximum value
     * @param   numbers
     * @return
     */
    private int findMaxPossibility(List<Double> numbers) {
        double max = numbers.get(0);
        int pos = 0;

        for (int i = 0; i < numbers.size(); i++) {
            double num = numbers.get(i);
            if (max < num) {
                max = num;
                pos = i;
            }
        }

        if(max == 0)
            return -1;

        return pos;
    }

    /**
     * @brief
     * @param   word
     * @param   one
     * @param   two
     * @return
     */
    private List<Double> calculatePossibility(int word, int one, int two) {
        List<Double> possibility = new ArrayList<>();

        for (int three = 0; three < uniGramData.size(); three++)
            possibility.add( (double)emission.get(word).get(three) * tranmission.get(one).get(two).get(three) );

        return possibility;
    }

    /**
     * @brief
     * @param   words
     * @return
     */
    public String[] tag(String[] words) {
        List<Double> possibility;
        //store locations of most similary labels
        List<Integer> location = new ArrayList<>();
        //label one and two
        int one = 0, two = 0;
        double pos = 1;
        //Other tag location
        final int Other_Tag_Location;
        //TODO  get rid of this for optimization
        List<String> initialLabels = new ArrayList<>(uniGramData.keySet());

        initialEmission(Arrays.asList(words));

        Other_Tag_Location = initialLabels.indexOf(OTHER_COPUS_TAG);

        for (int word = 0; word < words.length; word++) {
            switch (word) {
                case 0:
                    possibility = calculatePossibility(0, 0, 0);
                    two = findMaxPossibility(possibility);
                    if(two != -1) {
                        pos *= possibility.get(two);
                        location.add(two);
                    } else {
                        two = Other_Tag_Location;
                        location.add(two);
                    }

                    break;
                case 1:
                    possibility = calculatePossibility(1, 0, two);
                    one = findMaxPossibility(possibility);
                    if (one != -1) {
                        pos *= possibility.get(one);
                        location.add(one);
                    } else {
                        one = Other_Tag_Location;
                        location.add(one);
                    }

                    break;
                default:
                    possibility = calculatePossibility(word, one, two);
                    one = two;
                    two = findMaxPossibility(possibility);
                    if(two != -1) {
                        pos *= possibility.get(two);
                        location.add(two);
                    } else {
                        two = Other_Tag_Location;
                        location.add(two);
                    }

                    break;
            }
            possibility.clear();
        }

        String[] ret = new String[location.size()];
        for(int i = 0; i < location.size(); i++)
            ret[i] = initialLabels.get(location.get(i));

        return ret;
    }

}
