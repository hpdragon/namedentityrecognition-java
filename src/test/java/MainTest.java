import org.hpdragon.naturallanguageprocessing.semantics.namedentityrecognition.NamedEntityRecognition;

import java.io.File;
import java.io.IOException;

public class MainTest {

    private NamedEntityRecognition ner;

//    private static String NEED_TO_LEARN = "src/main/resources/needToLearn/train_data.txt";
    private static String NEED_TO_LEARN = "src/main/resources/needToLearn/file_train_tagged.txt";

    private static String LEARNED = "src/main/resources/learned/learned.txt";
//    private static String LEARNED = "src/main/resources/learned/1000trained.txt";

//    private static boolean TAGGING_MODE = false;
    private static boolean TAGGING_MODE = true;

    public static void normalTest(NamedEntityRecognition ner) {
        try {
            switch (Boolean.toString(TAGGING_MODE)) {
                case "false":
                    //learn
                    System.out.println("Learning Mode");
                    System.out.println();

                    ner.learn(new File(NEED_TO_LEARN));
                    ner.saveLearnedData(new File(LEARNED));

                    break;
                case "true":
                    //tag
                    System.out.println("Tagging Mode");
                    System.out.println();

                    ner.loadMemory(new File(LEARNED));

//                    String sentence = "ddawjt baso thuwsc lusc muwowfi giowf";
//                    String sentence = "hejn giowf lusc muwowfi moojt giowf muwowfi lawm phust";
                    String sentence = "ee ku ddawjt baso thuwsc vafo lusc hai muwowi giowf boosn muwowi phust nha";
                    String[] words = sentence.split(" ");

                    String[] tag = ner.tag(words);
                    for (int i = 0; i < tag.length; i++) {
                        System.out.println(words[i] + "\t" + tag[i]);
                    }

                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void learn(NamedEntityRecognition ner, String filePath) {
        System.out.println("Learning Mode");
        System.out.println();

        try {
            ner.learn(new File(filePath));
            ner.saveLearnedData(new File(LEARNED));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void tag(NamedEntityRecognition ner, String sentence) {
        System.out.println("Tagging Mode");
        System.out.println();

        try {
            ner.loadMemory(new File(LEARNED));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String[] words = sentence.split(" ");

        String[] tag = ner.tag(words);
        for (int i = 0; i < tag.length; i++) {
            System.out.println(words[i] + "\t" + tag[i]);
        }
    }

    public static void main(String args[]) {
        System.out.println("Named Entity Recognition");

        NamedEntityRecognition ner = new NamedEntityRecognition();

        switch (args.length) {
            case 0:
                MainTest.normalTest(ner);

                break;
            case 1:
                break;
            default:
                switch (args[0]) {
                    case "--learn":
                    case "-l":
                        System.out.println("\nStart time: " + System.currentTimeMillis() + "\n");
                        learn(ner, args[1]);
                        System.out.println("Stop time: " + System.currentTimeMillis() + "\n");

                        break;
                    case "--tag":
                    case "-t":
                        String sentence = "";
                        for (int i = 1; i < args.length; i++)
                            sentence += args[i] + " ";
                        tag(ner, sentence);

                        break;
                    default:
                        System.out.println("Select between --learn or --tag");
                        break;
                }

                break;
        }

    }

}
